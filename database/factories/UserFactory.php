<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $levels = [
        'rookie',
        'amateur',
        'pro'
    ];


    return [
        'name' => $faker->name,
        'level' => $levels[rand(0, 2)],
        'score' => rand(5, 100),
        'Suspected' => rand(0, 1),
    ];
});
