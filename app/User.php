<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'level', 'score', 'Suspected'
    ];
    /*
     * Cast attributes
     */
    protected $casts = [
        'Suspected' => 'boolean'
    ];
    /*
     * Hidden attribute (array serialization)
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @param $query
     * @param $request
     * @return mixed
     */
    public function scopeGetPlayers($query, $request)
    {
        $start = $request->get('start', 0);
        $n = $request->get('n', 1);
        $level = $request->get('level', null);
        $search = $request->get('search', null);

        return $query->take($n)
            ->skip($start)
            ->when($level, function ($q) use ($level) {
                $q->whereLevel($level);
            })
            ->when($search, function ($q) use ($search) {
                return $q->orWhere('id', 'like', '%' . $search . '%')
                    ->orWhere('name', 'like', '%' . $search . '%')
                    ->orWhere('level', 'like', '%' . $search . '%')
                    ->orWhere('score', 'like', '%' . $search . '%');
            })
            ->get();

    }
}
