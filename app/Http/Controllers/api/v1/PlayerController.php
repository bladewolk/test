<?php

namespace App\Http\Controllers\api\v1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlayerController extends Controller
{
    /**
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $playes = User::getPlayers($request);
        $total = User::count();

        return response($playes)
            ->header('x-total', $total);
    }
}
